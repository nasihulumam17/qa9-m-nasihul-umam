<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Medicaid_programs</name>
   <tag></tag>
   <elementGuidId>66656da1-3243-498f-9c60-1b8e3a8edc70</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='radio_program_medicaid']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#radio_program_medicaid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>5876f559-fc5d-46b7-95ba-53072560e50c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>34a10d07-f658-47f1-aa4c-1d13c82df71e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>programs</value>
      <webElementGuid>0e4d926b-d40f-412b-8e6a-9ab4f7ae38e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>radio_program_medicaid</value>
      <webElementGuid>ac097b6d-c841-4cbe-a79d-4896247b3155</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Medicaid</value>
      <webElementGuid>4371756d-4ec9-4174-b203-5379843400fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;radio_program_medicaid&quot;)</value>
      <webElementGuid>7b14b99f-5abb-420c-a516-cbde824e9bd2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='radio_program_medicaid']</value>
      <webElementGuid>373b7ad4-e516-4b8d-9fa9-17591da46435</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[3]/div/label[2]/input</value>
      <webElementGuid>23b19c21-5841-41f7-b428-4c500f974974</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label[2]/input</value>
      <webElementGuid>13b06950-b214-4f22-864c-4bd74c42eab9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'radio' and @name = 'programs' and @id = 'radio_program_medicaid']</value>
      <webElementGuid>8d1fb240-1d68-49ac-bd27-98240f8498f6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
